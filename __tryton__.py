# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'German chart of accounts 2014 for statement on excess of '
            'receipts over expenses',
    'name_de_DE': 'Deutscher Kontenrahmen 2014 für '
            'Einnahmen-Überschuß Rechnung',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Financial and accounting module (only for Germany):
    - Provides chart of accounts for statement on excess of
      receipts over expenses.
    - Tax year 2014.
''',
    'description_de_DE': '''Buchhaltungsmodul (für Deutschland):
    - Stellt den Kontenrahmen für Einnahmen-/Überschußrechnung zur
      Verfügung.
    - Steuerjahr 2014
''',
    'depends': [
        'account_de_euer_2013_timeline',
        'account_timeline_tax_de',
        'account_timeline_tax_de_cash_basis',
        'account_option_party_required',
    ],
    'xml': [
        'account_de_euer_2014.xml',
        'account_de_euer_2013_successors.xml',
    ],
    'translation': [
    ],
}
